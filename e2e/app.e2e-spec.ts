import { PracticeAngularPage } from './app.po';

describe('practice-angular App', function() {
  let page: PracticeAngularPage;

  beforeEach(() => {
    page = new PracticeAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
