import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {

  user:User;
  @Output() deleteEvent = new EventEmitter<User>();
  @Output() editEvent = new EventEmitter<User>();
  tempUser:User = {name:null, email:null}
  isEdit:Boolean = false;
  editButtonText = 'Edit';
  
  constructor() { }

  sendDelete(){
    this.deleteEvent.emit(this.user);
  }

  sendEdit() {
    this.isEdit = false;
    this.user.name = this.tempUser.name;
    this.user.email = this.tempUser.email;
    this.editButtonText = 'Edit';

}

  toggleEdit () {
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText ='Edit';
    if(!this.isEdit){
    	this.editEvent.emit(this.user);
    } 
  }

  ngOnInit() {
  }

}