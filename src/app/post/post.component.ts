import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {

  post:Post;
  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>();
  tempPost:Post = {title:null, body:null}
  isEdit:Boolean = false;
  editButtonText = 'Edit';
  
  constructor() { }

  sendDelete(){
    this.deleteEvent.emit(this.post);
  }

  sendEdit() {
    this.isEdit = false;
    this.post.title = this.tempPost.title;
    this.post.body = this.tempPost.body;
    this.editButtonText = 'Edit';

}

  toggleEdit () {
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText ='Edit';
    if(!this.isEdit){
    	this.editEvent.emit(this.post);
    } 
  }

  ngOnInit() {
  }

}
