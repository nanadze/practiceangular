import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class PostsService {

 postsObservable;

  getPosts (){
    this.postsObservable = this.af.database.list('/posts/').map(
            posts => {
        posts.map(
          post=> {
            post.postUsers = [];
            for(var p in post.users){
             post.postUsers.push(
                this.af.database.object('/users/' + p)
              )
            }
          }
        );
        return posts;
      }
    );
    return this.postsObservable;
  }
  
  addPost(post){
   this.postsObservable.push(post);
  }

  updatePost(post){
   let postKey= post.$key;
   let postData = {body:post.body,title:post.title};
   this.af.database.object('/posts/' + postKey).update(postData);
  }

  deletePost(post){
   let postKey= post.$key;
   this.af.database.object('/posts/' + postKey).remove();
  }

  constructor(private af:AngularFire) { }

}

