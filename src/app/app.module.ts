import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.Service';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.Service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostFormComponent } from './post-form/post-form.component';
import { UserComponent } from './user/user.component';
import { UserFormComponent } from './user-form/user-form.component';

export const firebaseConfig = {
  apiKey: "AIzaSyAftnj5pvd6qRs4pxemBt9dNo27NWexNFY",
  authDomain: "practiceangular-251cb.firebaseapp.com",
  databaseURL: "https://practiceangular-251cb.firebaseio.com",
  storageBucket: "practiceangular-251cb.appspot.com",
  messagingSenderId: "883509922310"
}

const appRoutes:Routes = [
  {path:'users',component:UsersComponent},
  {path:'posts',component:PostsComponent},
  {path:'',component:UsersComponent},
  {path:'**',component:PageNotFoundComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    PostFormComponent,
    UserComponent,
    UserFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [PostsService, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
